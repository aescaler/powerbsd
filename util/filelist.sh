#!/usr/bin/env bash
# @File:     filelist.sh
# @Created:  2021-11-15 23:35:21
# @Modified: 2021-11-15 23:40:57
# @OA:       aescaler
# @CA:       aescaler
# @Mail:     aj@aod.sh
# @Copy:     Copyright © 2021 Antonio Escalera <aj@aod.sh>

# gets recently modified files excluding some files and directories 
# that don't commonly contain configuration.

doas find / \
  -type f \
  -not -name "*.db" \
  -not -name "*.dat" \
  -not -name "*.cache" \
  -not -path "/compat/*" \
  -not -path "/home/*" \
  -not -path "/usr/home/*" \
  -not -path "/usr/local/lib/*" \
  -not -path "/usr/local/share/licenses/*" \
  -not -path "/usr/ports/*" \
  -not -path "/var/backups/*" \
  -not -path "/var/db/*" \
  -not -path "/var/log/*" \
  -not -path "/var/run/*" \
  -not -path "/var/spool/*" \
  -mtime -4 \
  | tee files-$(date -Iseconds).log

