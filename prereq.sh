#!/usr/bin/env bash
# @File:       prereq.sh
# @Created:    2020-12-05 18:03:12
# @Modified:   2020-12-05 18:43:25
# @OA:         aescaler
# @CA:         aescaler
# @Mail:       aj@aod.sh
# @Copy:       Copyright © 2019 Antonio Escalera <aj@aod.sh>
# @Use:        Authorized for use by aescaler.

fetch_prerequisites() {
  
  if [ ! -d "${TOOL_DIR}" ]; then
    mkdir -p "${TOOL_DIR}"
  fi

  cd "${TOOL_DIR}"
  if [ ! -d render ]; then
    git clone https://github.com/VirtusLab/render
    cd render
  else
    cd render
    git pull || true
  fi
  
  gmake build
  cd "${ROOT_DIR}"

}
