#!/usr/bin/env bash
# @File:       build.sh
# @Created:    2020-12-05 21:45:08
# @Modified:   2020-12-05 21:45:44
# @OA:         aescaler
# @CA:         aescaler
# @Mail:       aj@aod.sh
# @Copy:       Copyright © 2019 Antonio Escalera <aj@aod.sh>
# @Use:        Authorized for use by aescaler.

set -ex

cd "$(dirname "$0")/../"

# shellcheck disable=SC2068
version() { IFS="."; printf "%03d%03d%03d\\n" $@; unset IFS;}

if [ "$(version "${CURR_GO_VERSION#go}")" -lt "$(version "${MINIMUM_GO_VERSION}")" ]; then
  echo "Go version should be greater or equal to: ${MINIMUM_GO_VERSION}"
  exit 1
fi

FILE="mkimg.sh"
INPUT_DIR=tmpl/
OUTPUT_DIR=.rendered/
TOOL_DIR=tool
CFG_FILE="${1}"
ROOT_DIR="$(dirname $(readlink -f $0))"

containerFunc() {
  GOARCH="${ARCH}" GOOS="${OS}" \
    go build \
    -a \
    -v \
    -x \
    -trimpath \
    "${GOFLAGS}" \
    -o="${BUILD_PATH}"/"${APP}" \
    -ldflags "${LDFLAGS}" \
    -tags "${TAGS}"
  echo "[OK] Binary created successfully!"
}

containerGen() {
  podman run --rm \
    --env IS_CONTAINER=TRUE \
    -v "$PWD":/usr/src/"$(basename "$PWD")":z \
    -w /usr/src/"$(basename "$PWD")" \
    golang:1.14 \
    ./hack/build.sh
}

build() {
  if command -v go >/dev/null; then
    containerFunc
  else
    containerGen
  fi
}

# Build binary
if [ "${RELEASE}" = "true" ]; then
  LDFLAGS="${LDFLAGS} -s -w"
  TAGS="${TAGS} release"
  build 2>&1 | tee -a "${BUILD_PATH}"/make.log
  mv "${BUILD_PATH}"/make.log "${LOG_PATH}"/make.log
else
  build 2>&1 | tee -a "${BUILD_PATH}"/make.log
fi
