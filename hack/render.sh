#!/usr/bin/env bash
# @File:       render.sh
# @Created:    2020-12-05 22:07:52
# @Modified:   2020-12-06 20:19:34
# @OA:         aescaler
# @CA:         aescaler
# @Mail:       aj@aod.sh
# @Copy:       Copyright © 2019 Antonio Escalera <aj@aod.sh>
# @Use:        Authorized for use by aescaler.

set -ex

cd "$(dirname "$0")/../"

containerFunc() {
  git clone https://github.com/VirtusLab/render
  echo "[OK] Render completed successfully!" 
}

containerGen() {
  podman run --rm \
    --env IS_CONTAINER=TRUE \
    -v "$PWD":/usr/src/"$(basename "$PWD")":z \
    -w /usr/src/"$(basename "$PWD")" \
    golang:1.14 \
    ./hack/render.sh "${@}"
}

conditional() {
  if command -v render >/dev/null; then
    containerFunc "${@}"
  else
    containerGen "${@}"
  fi
}

# run go generate from inside the container
if [ "$RELEASE" = "true" ]; then
  conditional "${@}" 2>&1 | tee -a "${BUILD_PATH}"/make.log
else
  conditional "${@}" 2>&1 | tee -a "${BUILD_PATH}"/make.log
fi
